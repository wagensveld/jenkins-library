# Jenkins Shared Library

This repository hosts libraries I have written for Jenkins.

## Installation

In Jenkins go to `Manage Jenkins` -> `Configure System`, find the `Global Pipeline Libraries` section, click `Add`.

Use the following variables as a guide:

```
Name: wagensveld-jenkins
Default version: master
Load implicitly: false
Allow default version to be overridden: true
Include @Library changes in job recent changes: true
Cache fetched versions on master for quick retrieval: false

Retrieval method: Modern SCM
Source Code Management: Git
Project Repository: https://gitlab.com/wagensveld/jenkins-library
```

## Usage

If you didn't tick `Load implicitly` while setting up the pipeline add `@Library('wagensveld-jenkins') _` to the start of your pipline.

You can also specify a specific branch/tag: `@Library('wagensveld-jenkins@feature/branch') _` or `@Library('wagensveld-jenkins@1.0') _`

The libraries themselves live in the vars folder, to call them call their name during a stage in a pipeline, for example to use paramsDescription.groovy do something like the following Jenkinsfile:

```groovy
@Library('my-library') _

pipeline {
  agent any
  stages {
    stage('Pipeline Config') {
      steps {
        paramsDescription()
      }
    }
    stage('Deploy') {
      steps {
        echo 'Deploying...'
      }
    }
  }
}
```
