/*
Add paramsDescription() to a stage, and this will set the build description to the following format:

PARAMETER_KEY_1: PARAMETER_VALUE_1
PARAMETER_KEY_2: PARAMETER_VALUE_2
...

*/
def call() {
  def paramsStringBuilder = new StringBuilder()
  params.each { param ->
    paramsStringBuilder.append("${param.key}: ${param.value}\n")
  }
  currentBuild.description = paramsStringBuilder.toString()
}
